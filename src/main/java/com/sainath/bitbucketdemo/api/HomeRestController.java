package com.sainath.bitbucketdemo.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.Map;

@RequestMapping("/home")
public class HomeRestController {

    @GetMapping("/user")
    public Map<String, String> welcome() {
        Map<String, String> map = new HashMap<>();
        map.put("user", "Sainath Parkar");
        return map;
    }
    
    @GetMapping("/user/welcome")
    public String welcomeMsg() {
    	return "Welcome back";
    }
}
